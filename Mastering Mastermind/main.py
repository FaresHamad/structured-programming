import random

"""de indeling van functie lijkt me goed.
    maar het was niet duidelijk wat zeg maar moet invoeren, als ik de code run krijg ik meteen (voer je gok in: ) maar ik wist wat voor gok ik moet invoeren nummers,
     kleuren of letters dus dat was beetje onduidelijk voor mij.
     ik zag geen commentaar onder de functies en dat vind best wel belangrijk.
     en nog een ding als je zeg een apparte functie voor feedback maakt dan kan je die straks gebruiken bij de algoritme maken.
     verder ziet ie er netjes uit veel succes (: """

def intChecker(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


def answerGetter():
    numAnswer = [int(x) for x in (str(random.randrange(1000, 9999)))]
    return numAnswer


def guessGetter():
    numGuess = (input("Voer je gok in:"))
    if not len(numGuess) == 4 or not intChecker(numGuess):
        while True:
            numGuess = (input("Ongeldige invoer, je gok moet uit 4 cijfers bestaan. Probeer opnieuw: "))
            if len(numGuess) == 4 and intChecker(numGuess):
                break

    numGuess = [int(x) for x in str(numGuess)]
    return numGuess


guess = guessGetter()
answer = answerGetter()
attemptCount = 0

if guess == answer:
    print('Sjow! Je hebt het in 1 keer goed geraad, je bent echt een Mastermind!')

else:
    while guess != answer:

        correctCount = 0
        correctList = []
        attemptCount += 1

        if attemptCount > 10:
            print(
                f'Helaas pindakaas, Je hebt geen pogingen meer. Het antwoord was {answer}.'
                f' run de pogramme opnieuw als je nog een poging wilt wagen.')
            break

        for x in range(0, 4):
            if guess[x] == answer[x]:
                correctCount += 1
                correctList.append(guess[x])
            else:
                continue

        if correctCount < 4 and correctCount != 0:
            print(
                f'Niet helemaal het nummer. Maar je had wel {correctCount} cijfer(s) goed! Deze cijfers in je '
                f'invoer zijn correct. {sorted(correctList)} \n')
            guess = guessGetter()

        elif correctCount == 0:
            print(f'Geen van de nummers in je invoer zijn correct.')
            guess = guessGetter()

        if guess == answer:
            print(f'Je bent echt een mastermind! Je had {attemptCount} pogingen nodig')
