import random

lst = [1, 2.5, 6, 7.1, 23, 76.1, 56.1, 90, 102.1, 210, 10012]


def piramideMaker(direction):
    """Opdracht 1 - Pyramide"""
    p_size = int(input('Hoe groot wil je de piramide: '))
    p_string = '*'
    i = 0

    print(f'\nMet 2 for loops:')
    for x in range(1, p_size + 1):
        if direction.lower() == 'normal':
            print(x, (p_string * x))
        elif direction.lower() == 'inverted':
            print((p_size - x) + 1, (p_string * ((p_size - x) + 1)))
        if x == p_size:
            for y in range(p_size - 1, -1, -1):
                if y == 0:
                    break
                if direction.lower() == 'normal':
                    print(y, (p_string * y)),
                elif direction.lower() == 'inverted':
                    print((p_size - y) + 1, (p_string * ((p_size - y) + 1)))

    print(f'\nMet 2 while loops:')
    if i < p_size:
        x = True
        while x:
            i += 1
            if i == p_size:
                x = False
            if direction.lower() == 'normal':
                print(i, (p_string * i))
            elif direction.lower() == 'inverted':
                print((p_size - i) + 1, (p_string * ((p_size - i) + 1)))
        while not x:
            i -= 1
            if i == 0:
                break
            if direction.lower() == 'normal':
                print(i, (p_string * i))
            elif direction.lower() == 'inverted':
                print((p_size - i) + 1, (p_string * ((p_size - i) + 1)))


def stringChecker():
    """Opdracht 2 - Tekstcheck"""
    string_1 = str(input('String 1: '))
    string_2 = str(input('String 2: '))
    i = 0

    if len(string_1) > len(string_2):
        largerString = string_1
        smallerString = string_2
    else:
        largerString = string_2
        smallerString = string_1

    for x in smallerString:
        i += 1
        for y in largerString:
            if not x == y:
                print(f'Het eerste verschil zit op index: {i}')
                break


def listCounter(lst, num):
    """Opdracht 3a - Lijstcheck"""
    i = 0
    for x in lst:
        if x == num:
            i += 1
    return i


def listDifferentiator(lst):
    """Opdracht 3b - Lijstcheck"""
    lst = [1, 2.5, 6, 7.1, 23, 76.1, 56.1, 90, 102.1, 210, 10012]
    i = 0
    difference = 0
    while True:
        if i == (len(lst) - 1):
            return difference

        if lst[i] < lst[i + 1]:
            tempDifference = lst[i + 1] - lst[i]
        else:
            tempDifference = lst[i] - lst[i - 1]
        i += 1

        if tempDifference > difference:
            difference = tempDifference


def oneZeroCounter():
    """Opdracht 3c - Lijstcheck"""
    randomlist = []

    for i in range(0, 24):
        n = random.randint(0, 1)
        randomlist.append(n)

    print(randomlist)
    oneCount = listCounter(randomlist, 1)
    zeroCount = listCounter(randomlist, 0)

    if oneCount > zeroCount and zeroCount <= 12:
        return True
    else:
        return False


def palindromeChecker(string, option):
    """Opdracht 4_1 - Palindroom"""
    if option == 'reverse':
        """Met de ingebouwede reverse functie"""
        for_str = string.lower()
        rev_str = reversed(string.lower())
    elif option == 'custom':
        """Met eigen 'reverse' functie"""
        for_str = string.lower()
        rev_str = string.lower()[::-1]

    if list(for_str) == list(rev_str):
        return True
    else:
        return False


def lstSorter(old_list):
    """Opdracht 5 - Sorteren"""
    new_list = []
    while old_list:
        minimum = old_list[0]
        for x in old_list:
            if x < minimum:
                minimum = x
        new_list.append(minimum)
        old_list.remove(minimum)
    return new_list


def lstAverage(lst):
    """Opdracht 6 - Gemiddelde berekenen"""
    lstAvg = (sum(lst)) / (len(lst))
    return lstAvg


def lstAverageInput():
    """Opdracht 6 input - Gemiddelde berekenen"""
    lstInput = input('Voer een list in: (als: 1,23,4,5,6) ').split(',')
    lst = list(map(int, lstInput))
    lstAvg = (sum(lst)) / (len(lst))
    return lstAvg


def randomGuesser(start, end):
    """Opdracht 7 - Random"""
    selectedNumber = random.randint(start, end)
    while True:
        guessNumber = int(input(f'Raad een nummer tussen {start} en {end}: '))
        if guessNumber == selectedNumber:
            print(f'Raak! {selectedNumber} is correct!')
            break


def fileCompressor():
    """Opdracht 8 - Compressie"""
    with open("txt.txt", "r") as f:
        lines = f.readlines()
        clean_lines = [l.strip() for l in lines if l.strip()]
    with open("txtCompressed.txt", "w") as f:
        f.writelines('\n'.join(clean_lines))


def cyclicShifting(string, n):
    """Opdracht 9 - Cyclisch verschuiven"""
    return string[n:] + string[:n]


def fibonaci():
    """Opdracht 10 - Fibonaci"""

    def recursive(n):
        if n <= 1:
            return n
        else:
            return recursive(n - 1) + recursive(n - 2)

    f = int(input('Hoeveel fn elementen: '))
    if f <= 0:
        f = int(input('ongeldige invoer: '))
    else:
        for x in range(f):
            print(recursive(x))


def ceaserCipher():
    """Opdracht 11 - Caesarcijfer"""
    plainText = input("Geef een tekst: ")
    shift = int(input("Geef een rotatie: "))
    encrypted_text = ''
    for i in range(len(plainText)):
        if plainText[i] == ' ':
            encrypted_text = encrypted_text + plainText[i]
        elif plainText[i].isupper():
            encrypted_text = encrypted_text + chr((ord(plainText[i]) + shift - 65) % 26 + 65)
        else:
            encrypted_text = encrypted_text + chr((ord(plainText[i]) + shift - 97) % 26 + 97)
    return encrypted_text


def fizzBuzz():
    """Opdracht 12 - FizzBuzz"""
    for x in range(1, 100):
        if (x % 5) == 0 and (x % 3) == 0:
            print('fizzbuzz')
        elif (x % 5) == 0:
            print('buzz')
        elif (x % 3) == 0:
            print('fizz')
        else:
            print(x)
